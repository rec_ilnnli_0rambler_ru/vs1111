﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

    //   Settings instans = new Settings();   //для запуска програм инстанцировать -- Settings
    //   instans.Settingsaa();                // и вызвать класс Settingsaa
using System;
using System.Collections.Generic;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    class Settings          
    {
        private IParkingService f = new ParkingService();
        public void Settingsaa()
        {
            TransactionInfo.Sum = 0;
            Console.WriteLine("введите баланс парковки");
            TransactionInfo.Sum = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine("вместимость паркинга");
            Parking.lensparcs = f.GetCapacity();

            TimerService ds = new TimerService();    
            ds.Interval = 5000;   
            ds.Start();
            while (true)
            {
                Console.WriteLine("нажмите цыфру для слудующей опирации");
                Console.WriteLine("1 добавить тр, 2 удалить тр, 3 показать баланс паркинга , 4 пополнить щет автомобилю");
                int ff = Convert.ToInt32(Console.ReadLine());
                if (ff == 1)
                {
                    if (Parking.lensparcs > Parking.numbers.Count)  
                    {
                        string id = "000000";
                        bool s = true;
                        const string zzz = "[A-Z][A-Z][0-9][0-9][0-9][0-9][A-Z][A-Z]";        
                        var sss = new Regex(zzz); // 
                        while (s)
                        {
                            Console.WriteLine("введите ид автомобиля");
                            id = Console.ReadLine();
                            s = !sss.IsMatch(id);
                        }
                        Console.WriteLine("баланс авто");
                        decimal Balance = Convert.ToDecimal(Console.ReadLine());
                        Console.WriteLine("введите тип авто : легковой = 1, грузовик = 2, автобус = 3, мотоцикл =4,");
                        int d = Convert.ToInt32(Console.ReadLine());
                        VehicleType VehicleTy = (VehicleType)d;
                        f.AddVehicle(new Vehicle(id, VehicleTy, Balance));
                    }
                }
                else if (ff == 2)
                {
                    Console.WriteLine("введите id -обект который нужно удалить");
                    f.RemoveVehicle(Console.ReadLine());
                }
                else if (ff == 3)
                {
                    Console.WriteLine($"Balance  паркинга - {TransactionInfo.Sum }");
                    Console.WriteLine("количество занятых мест");
                    {
                        Console.WriteLine(Parking.numbers.Count);
                    }
                    Console.WriteLine($"сколько свободных мест - {f.GetFreePlaces()}");
                    //   Console.WriteLine("транспортные стредства : ");
                    //  foreach (Vehicle c in Parking.numbers)
                    //   {
                    //      Console.WriteLine($"id - {c.Id}");
                    //      Console.WriteLine($"Balance - {c.Balance}");
                    //      Console.WriteLine($"тип авто - { c.VehicleType}");
                    //  }
                }
                else if (ff == 4)
                {
                    Console.WriteLine("введите id автомобиля");
                    string vehicleId = Console.ReadLine();
                    Console.WriteLine("введите сумму проплаты");
                    decimal sum = Convert.ToDecimal(Console.ReadLine());
                    f.TopUpVehicle(vehicleId, sum);
                }
                else { Console.WriteLine("вы ввели недопустимое число, пустых мест для мажин нет"); }
            }
        }
    }
}
