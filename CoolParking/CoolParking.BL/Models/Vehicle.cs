﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
namespace  CoolParking.BL.Models
{
   public class Vehicle  
    {
        private readonly string id;
        private readonly VehicleType vehicleType;
        private decimal balance;
        public Vehicle(string Id, VehicleType VehicleType, decimal Balance)
        {
            this.id = Id;
            this.vehicleType = VehicleType;
            this.balance = Balance;
        }
        public string Id { get { return id; } }  
        public VehicleType VehicleType { get { return vehicleType; } } 
        public decimal Balance { get { return balance; } set { balance = value; } }  
    }
}