﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    class TimerService : ITimerService  

    {

        public ILogService hhh = new LogService();

        Timer t = new Timer();        
        public double Interval       
        {
            get { return t.Interval; }
            set { t.Interval = value; }
        }
        public event ElapsedEventHandler Elapsed    
        {
            add { t.Elapsed += value; }
            remove { t.Elapsed -= value; }
        }
        public void Dispose()
        {
            Dispose();
        }
        public void Start()
        {
            Elapsed += OnTimer;
            t.Enabled = true;
            t.AutoReset = true;

            void OnTimer(object sender, ElapsedEventArgs args)
            {
                int fff = 0;
                fff++;
                while (fff == 12) { hhh.Write(Convert.ToString(TransactionInfo.Sum)); fff = 0; } // замер 60 секунд
                foreach (Vehicle cd in Parking.numbers)
                {
                    if (cd.Balance <= 0) { cd.Balance -= (decimal)2.5; TransactionInfo.Sum += (decimal)2.5; break; }
                    if (cd.VehicleType == VehicleType.автобус)
                    {
                        cd.Balance -= (decimal)3.5;
                        TransactionInfo.Sum += (decimal)3.5;
                        break;
                    }
                    else if (cd.VehicleType == VehicleType.грузовик)
                    {
                        cd.Balance -= 5;
                        TransactionInfo.Sum += 5;
                        break;
                    }
                    else if (cd.VehicleType == VehicleType.легковой)
                    {
                        cd.Balance -= 2;
                        TransactionInfo.Sum += 2;

                        break;
                    }
                    else if (cd.VehicleType == VehicleType.мотоцикл)
                    {
                        cd.Balance -= 1;
                        TransactionInfo.Sum += 1;
                        break;
                    }
                }
            }
            t.Start();
        }
        public void Stop()
        {
            t.Stop();
        }
    }
}
