﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using System.IO;

namespace CoolParking.BL.Services

{
   class LogService : ILogService   
    {
        public string logPath = "text.txt";
        public string LogPath { get { return logPath; } }  

        public string Read()      
        {
            StreamReader reader = File.OpenText("Text.txt");   
            string input;
            
            while ((input = reader.ReadLine()) != null)
            {
                // Console.WriteLine(input);             
            }
            reader.Close();    
            return input;
        }
        public void Write(string logInfo)     
        {
            var file2 = new FileStream("text.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            var writer = new StreamWriter(file2, Encoding.GetEncoding(1251));
            writer.WriteLine(logInfo);        
            writer.Write(writer.NewLine);     
            writer.Close();                
        }
    }
}
