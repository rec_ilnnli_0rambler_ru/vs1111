﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    class ParkingService : IParkingService     
    {
        private bool disposedValue;

        public void AddVehicle(Vehicle vehicle)  
        {
            Parking.numbers.Add(vehicle);
        }
        public decimal GetBalance()
        {
            throw new NotImplementedException();
        }
        public int GetCapacity()                     
        {
            int lensparc = Convert.ToInt32(Console.ReadLine()); 
            Parking.numbers.Capacity = lensparc;
            return lensparc;
        }

        public int GetFreePlaces()                  
        {
            return Parking.lensparcs - Parking.numbers.Count ;
        }

        public TransactionInfo[] GetLastParkingTransactions() 
        {
            throw new NotImplementedException();
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()  
        {
            return Parking.numbers.AsReadOnly ();
        }
        public string ReadFromLog()      
        {
            throw new NotImplementedException();
        }
        public void RemoveVehicle(string vehicleId)       
        {
            foreach (Vehicle aaaa in Parking.numbers)
            {
                if (aaaa.Id == vehicleId)
                {
                    Console.WriteLine(aaaa.Id);
                    Parking.numbers.Remove(aaaa);
                    break;
                }
            }
        }
        public void TopUpVehicle(string vehicleId, decimal sum)  
        {
            foreach (Vehicle aaaa in Parking.numbers)
            {
                if (aaaa.Id == vehicleId)
                {
                    Console.WriteLine($"id - {aaaa.Id}");
                    Console.WriteLine($"Balance - {aaaa.Balance}");
                    Console.WriteLine($"тип авто - { aaaa.VehicleType}");
                    aaaa.Balance += sum;
                    Console.WriteLine($"щбщая сумма - {aaaa.Balance}");
                    break;
                }
            }
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {  
                }
                disposedValue = true;
            }
        }
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
